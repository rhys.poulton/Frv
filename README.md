# Frv (Frame Vector Library)

Frv is a utility library dedicated to vector manipulation. The vectors used in
this package are the Frame Library vectors (FrVect) described in the frame
specification. The only part frame library part used in this library is the
vector definition from the FrameL.h.

This library is intended to simplify the access of date for the various vectors.
It provides most of the basic tools to do vector algebra and basic signal
processing.

This code is written in C. It is thread safe and could be easily used in C or
C++ software.
