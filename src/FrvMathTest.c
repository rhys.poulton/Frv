/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*-----------------------------------------------------------*/
/* test the FrvBasic code */
/*-----------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "FrvMath.h"
  
/*---------------------------------------------------------------------------*/
void testMath(FrVect *vect1, FrVect *vect2, FrVect *vectOut)
/*---------------------------------------------------------------------------*/
{struct FrVect *vect3, *vect;
 double delta, previous, mean, rms;

 printf("--- Test FrvMath with the following vectors ----------\n");
 FrVectDump(vect1, stdout, 2);
 FrVectDump(vect2, stdout, 2);

 printf("\n--- Test FrvAdd with vectOut = NULL----------\n");
 vect = FrvAdd(vect1, vect2, NULL, "vect3");
 FrVectDump(vect, stdout, 2);

 printf("--- Test FrvAdd with vectOut != NULL----------\n");
 vect = FrvAdd(vect1, vect2, vectOut, "vect3");
 FrVectDump(vect, stdout, 2);

 printf("--- Test FrvAdd with vectOut = vect2----------\n");
 vect = FrvAdd(vect1, vect2, vect2, NULL);
 FrVectDump(vect, stdout, 2);

 printf("--- Test FrvCombine2 with vect1+vect2/1.5----------\n");
 vect = FrvCombine2(1., vect1, 1./1.5, vect2, NULL, "vect1+vect2/1.5");
 FrVectDump(vect, stdout, 2);

 printf("--- Test FrvCombine with vect1+vect2/1.5----------\n");
 vect3 = FrvCombine(2, 1., vect1, 1./1.5, vect2, NULL, "vect3=vect1+vect2/1.5");
 FrVectDump(vect3, stdout, 2);

 printf("--- Test FrvCombine with vect1+1.5*vect2+.5*vect3----------\n");
 vect = FrvCombine(3, 1., vect1, 1./1.5, vect2, 0.5, vect3, vectOut, NULL);
 FrVectDump(vect, stdout, 2);

 printf("--- Test FrvDelta---------\n");
 printf(" return with previous == NULL: %d\n",FrvDelta(vect,&delta,NULL));
 printf(" delta = %f\n",delta);
 previous = 10.;
 printf(" return with previous != NULL: %d\n",FrvDelta(vect,&delta,&previous));
 printf(" delta = %f previous = %f\n",delta, previous);

 printf("\n--- Test FrvDivide Vect1/vect2----------\n");
 vect = FrvDivide(vect1, vect2, NULL,"vect1/vect2");
 FrVectDump(vect1, stdout, 2);
 FrVectDump(vect2, stdout, 2);
 FrVectDump(vect, stdout, 2);
 printf("--- Test FrvDivide vectOut = NULL newName = NULL----------\n");
 vect = FrvDivide(vect1, vect2, NULL, NULL);

 printf("\n--- Test FrvMult Vect1/vect2----------\n");
 vect = FrvMult(vect1, vect2, NULL,"vect1*vect2");
 FrVectDump(vect1, stdout, 2);
 FrVectDump(vect2, stdout, 2);
 FrVectDump(vect, stdout, 2);
 printf("--- Test FrvMult vectOut = vect2----------\n");
 vect = FrvMult(vect1, vect2, vect2, NULL);

 printf("\n--- Test FrvRms on vect1---------\n");
 printf(" return = %d\n",FrvRms(vect1,&mean, &rms));
 printf(" mean = %f rms = %f\n",mean, rms);

 printf("\n--- Test FrvScale Vect1----------\n");
 vect = FrvScale(2., vect1, NULL,"2.*vect1");
 FrVectDump(vect, stdout, 2);

 printf("\n--- Test FrvSub Vect1-vect2----------\n");
 vect = FrvSub(vect1, vect2, NULL,"vect1-vect2");
 FrVectDump(vect1, stdout, 2);
 FrVectDump(vect2, stdout, 2);
 FrVectDump(vect, stdout, 2);
 printf("--- Test FrvSub vectOut = NULL newName = NULL----------\n");
 vect = FrvSub(vect1, vect2, NULL, NULL);
 return;
}
/*---------------------------------------------------------------------------*/
int main(int argc, char *argv[])   /*-------------- main ----*/
{struct FrVect *vect1, *vect2, *vectOut;
 int nData, i;

 nData = 512;

 vect1   = FrVectNew1D("vect1",  FR_VECT_8R, nData,1.,"","");
 vect2   = FrVectNew1D("vect2",  FR_VECT_8R, nData,1.,"","");
 vectOut = FrVectNew1D("vectOut",FR_VECT_8R, nData,1.,"","");

 for(i=0; i<nData; i++) 
    {vect1->dataD[i] = 1. *i;
     vect2->dataD[i] = 0.5*i;}
 
 testMath(vect1, vect2, vectOut);

 printf("\n\n");

 nData = 32;
 vect1   = FrVectNew1D("vect1",  FR_VECT_2S, nData,1.,"","");
 vect2   = FrVectNew1D("vect2",  FR_VECT_2S, nData,1.,"","");
 vectOut = FrVectNew1D("vectOut",FR_VECT_2S, nData,1.,"","");

 for(i=0; i<nData; i++) 
    {vect1->dataS[i] = 1*i;
     vect2->dataS[i] = 2*i;}
 
 testMath(vect1, vect2, vectOut);

 printf("\n--- Test FrvModulus and FrvPhase----------\n");
 vect1   = FrVectNew1D("vect1",  FR_VECT_C8, nData,1.,"","");
 vect2   = FrVectNew1D("vect2",  FR_VECT_8R, nData,1.,"","");

 for(i=0; i<nData; i++) 
    {vect1->dataF[2*i]   = i*sin(i);
     vect1->dataF[2*i+1] = i*cos(i);}

 FrVectDump(vect1, stdout, 2);
 FrVectDump(FrvModulus(vect1,NULL,NULL),stdout,2);
 FrVectDump(FrvModulus(vect1,NULL,"myname"),stdout,2);
 FrVectDump(FrvPhase(vect1,NULL,NULL),stdout,2);
 printf("\n--- Test FrvPhase with vector type missmatch----------\n");
 FrVectDump(FrvPhase(vect1,vect2,NULL),stdout,2);
 
 
 return(0);}
