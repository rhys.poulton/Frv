/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*-----------------------------------------------------------------------------*/
/*  FrvMath.h by I. Ferrante, I. Fiori, F. Marion, B.Mours  Jan 06, 2004       */
/*-----------------------------------------------------------------------------*/

#ifndef FRVMATH
#define FRVMATH

#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif

#include "FrameL.h"
#include "FrvCopy.h"

#ifdef __cplusplus
extern "C" {
#endif

#define FRVTWOPI  6.2831853071795864769

struct FrvStat {

  double decay;
  double decayTime;
  long   nDecay;
  double dt;
  double meanA;
  double rmsA;
  double x2A;
  struct FrVect *mean;
  struct FrVect *rms;

};

typedef struct FrvStat FrvStat;

void     FrvStatFree(FrvStat *stat);
FrvStat* FrvStatNew();
FrvStat* FrvStatProc(FrvStat *stat, FrVect *vect);
FrvStat* FrvStatProcA(FrvStat *stat, FrVect *vect);
FrvStat* FrvStatProcV(FrvStat *stat, double input);
void     FrvStatReset(FrvStat *stat);
void     FrvStatSetDecay(FrvStat* stat, double decay);
void     FrvStatSetDecayTime(FrvStat* stat, double decayTime);

FrVect *FrvAdd(FrVect *vect1, FrVect *vect2, FrVect *vectOut, char* newName);
FrVect *FrvBias(FrVect *vect1, double bias, FrVect *vectOut);
FrVect *FrvCombine2(double s1, FrVect *vect1,
                    double s2, FrVect *vect2,
                    FrVect *vectOut, char* newName);
FrVect *FrvCombine(int nVect,
                   ...);
int     FrvDelta  (FrVect *vect1, double *delta, double *previous);
FrVect *FrvDivide (FrVect *vect1, FrVect *vect2,FrVect *vectOut,char* newName);
FrVect *FrvFlatten(FrVect *vect1, FrVect *vectOut); 

int     FrvFillGaus(FrVect *vect, double mean, double sigma);
double  FrvGaus(int i);
double  FrvUniform();

FrVect* FrvIntegrate(FrVect *inVect, int n, char* newName);
FrVect* FrvInterpolate(FrVect *vIn, FrVect *vOut, int n, int iStart, int nIn, char *option);
FrVect* FrvInterpolatePhase(FrVect *vIn, FrVect *vOut, int n, int iStart, int nIn, char *option);

#define FrvMean  FrvMean_is_obsolete_it_must_be_replaced_by_FrVectMean

FrVect *FrvModulus(FrVect *vect1, FrVect *vectOut, char *newName);
FrVect *FrvMult   (FrVect *vect1, FrVect *vect2,FrVect *vectOut,char* newName);
FrVect *FrvMultConj(FrVect *vect1, FrVect *vect2,FrVect *vectOut,char* newName);
FrVect *FrvPhase  (FrVect *vect1, FrVect *vectOut, char *newName);
int     FrvRms    (FrVect *vect1, double *mean, double *rms);
FrVect *FrvScale  (double scale, FrVect *vect1,FrVect *vectOut, char* newName);
FrVect *FrvSub    (FrVect *vect1,FrVect *vect2,FrVect *vectOut, char* newName);
FrVect *FrvZeroMean(FrVect *vect1,FrVect *vectOut); 

#ifdef __cplusplus
}
#endif

#endif
