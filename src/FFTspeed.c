/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*-----------------------------------------------------------*/
/* test the FFT code */
/*-----------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "srfftw.h"
 
#define NDATA 2048 
/*--------------------------------------------------------------------*/
int main(int argc, char *argv[])   /*-------------- main ----*/
{fftw_real input[NDATA], output[NDATA];
  rfftw_plan plan;           /* FFTW plan                                    */
 int i;

 printf(" size is:%d\n",fftw_sizeof_fftw_real());

 plan = rfftw_create_plan(NDATA,FFTW_REAL_TO_COMPLEX,FFTW_ESTIMATE);

 for(i=0; i<NDATA; i++) {input[i] = i;}

 for(i=0; i<10000; i++) {rfftw_one(plan, input, output);}

 printf(" FFTW word size was:%d\n",fftw_sizeof_fftw_real());
 return(0);
}
