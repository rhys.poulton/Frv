/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*-----------------------------------------------------------*/
/* test the Filter code */
/*-----------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Frv.h"
  
int main(int argc, char *argv[])   /*-------------- main ----*/
{
  FrFile* iFile = FrFileINew("/virgoData/ffl/raw.ffl");

  double duration = 100;
  double tStart1 =  1178172600;
  FrVect *b7 = FrFileIGetVectDN(iFile, "V1:LSC_B7_DC", tStart1, duration);
  FrVect *b8 = FrFileIGetVectDN(iFile, "V1:LSC_B8_DC", tStart1, duration);
  FrVectDump(b7, stdout, 2);
  FrVectDump(b8, stdout, 2);

  double dt = 2;
  FrvTF* tf  = FrvTFNewT("HANOPC", dt, 0);

  FrvTFProc(tf,  b7, b8);

  FrVectDump(tf->coherence, stdout, 2);

  FrVectSave(tf->coherence, "coherence.vect");

  return(0);
}
