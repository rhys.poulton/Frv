/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*- FrvCorr.c  May 31, 2001 by I. Ferrante (from Siesta file USignal.*) -----*/
#include "FrvCorr.h"
#include <string.h>
#include <math.h>

/*--------------------------------------------------------- FrvCorrFree --*/
void  FrvCorrFree(FrvCorr* corr)
/*---------------------------------------------------------------------------*/
/*Frees the space allocated to correlation                                   */
/*---------------------------------------------------------------------------*/
{

  if (corr->average!= NULL)
  FrVectFree(corr->average);

  if (corr->present!= NULL)
  FrVectFree(corr->present);

  free(corr->past1);
  free(corr->past2);

  free(corr);

  return;
}

/*---------------------------------------------------------- FrvCorrProc--*/
int FrvCorrProc(FrvCorr *corr, FrVect *vect1, FrVect *vect2)
/*---------------------------------------------------------------------------*/
/*   Calculates the correlation among vectors 1 and 2:                       */
/*        c_i=sum_k v1_k v2_(k+i)                                            */
/*   the correlation of the two vectors are stored in FrVect * corr->present */
/*   the correlation averaged over consecutive frames is in corr->average    */
/*   pointers to zero lag correlation are available in corr->present0        */
/*   and corr->average0. If corr->normalize==0 the unbiased correlation is   */
/*   calculated.                                                             */
/*   For the first call, past values are set to zero: they are recorded      */
/*   for subsequent calls and can be changed with FrvCorrSetIni              */
/*---------------------------------------------------------------------------*/
{double *v1, *v2;
 int lv , nData;
 int neww,oldw;
 int i,lag,imin,imax,maxlag;
 
 if((vect1== NULL)||(vect2==NULL)) return(1);
 if(corr  == NULL) return(2);

 if((corr->average == NULL)&&(corr->present == NULL))
   {if(FrvCorrInit(corr, vect1, vect2 ) >=1 ) return(3);}

   nData=vect1->nData;

 maxlag=corr->maxlag;
   lv=nData+maxlag;
   v1=(double*) malloc(lv*sizeof(double));
   v2=(double*) malloc(lv*sizeof(double));
 

 if     (vect1->type == FR_VECT_4R) 
 for (i=0; i<nData; i++ )  v1[i+maxlag] = vect1->dataF[i];
    else if(vect1->type == FR_VECT_8R) 
 for (i=0; i<nData; i++ )  v1[i+maxlag] = vect1->dataD[i];
    else if(vect1->type == FR_VECT_2S) 
 for (i=0; i<nData; i++ )  v1[i+maxlag] = vect1->dataS[i];
    else if(vect1->type == FR_VECT_4S) 
 for (i=0; i<nData; i++ )  v1[i+maxlag] = vect1->dataI[i];
    else if(vect1->type == FR_VECT_8S) 
 for (i=0; i<nData; i++ )  v1[i+maxlag] = vect1->dataL[i];
    else if(vect1->type == FR_VECT_C)  
 for (i=0; i<nData; i++ )  v1[i+maxlag] = vect1->data[i];
    else if(vect1->type == FR_VECT_2U) 
 for (i=0; i<nData; i++ )  v1[i+maxlag] = vect1->dataUS[i];
    else if(vect1->type == FR_VECT_4U) 
 for (i=0; i<nData; i++ )  v1[i+maxlag] = vect1->dataUI[i];
    else if(vect1->type == FR_VECT_8U) 
 for (i=0; i<nData; i++ )  v1[i+maxlag] = vect1->dataUL[i];
    else if(vect1->type == FR_VECT_1U) 
 for (i=0; i<nData; i++ )  v1[i+maxlag] = vect1->dataU[i];
 for (i=0;i<maxlag; i++ ) v1[i]=corr->past1[i];



 if     (vect2->type == FR_VECT_4R) 
 for (i=0; i<nData; i++ )  v2[i+maxlag] = vect2->dataF[i];
    else if(vect2->type == FR_VECT_8R) 
 for (i=0; i<nData; i++ )  v2[i+maxlag] = vect2->dataD[i];
    else if(vect2->type == FR_VECT_2S) 
 for (i=0; i<nData; i++ )  v2[i+maxlag] = vect2->dataS[i];
    else if(vect2->type == FR_VECT_4S) 
 for (i=0; i<nData; i++ )  v2[i+maxlag] = vect2->dataI[i];
    else if(vect2->type == FR_VECT_8S) 
 for (i=0; i<nData; i++ )  v2[i+maxlag] = vect2->dataL[i];
    else if(vect2->type == FR_VECT_C)  
 for (i=0; i<nData; i++ )  v2[i+maxlag] = vect2->data[i];
    else if(vect2->type == FR_VECT_2U) 
 for (i=0; i<nData; i++ )  v2[i+maxlag] = vect2->dataUS[i];
    else if(vect2->type == FR_VECT_4U) 
 for (i=0; i<nData; i++ )  v2[i+maxlag] = vect2->dataUI[i];
    else if(vect2->type == FR_VECT_8U) 
 for (i=0; i<nData; i++ )  v2[i+maxlag] = vect2->dataUL[i];
    else if(vect2->type == FR_VECT_1U) 
 for (i=0; i<nData; i++ )  v2[i+maxlag] = vect2->dataU[i];
 for (i=0;i<maxlag; i++ )  v2[i]=corr->past2[i];
         /*-------------------- set start time for all the series defined ---*/

 corr->average->GTime  = vect1->GTime;
 corr->present->GTime  = vect1->GTime;
 
 maxlag=corr->maxlag;
 for (lag = -maxlag; lag<=maxlag;lag++)
{
 corr->present->dataD[corr->maxlag+lag]=0;
 imin=0;
 if (lag>0) imin=-lag;

 imax=vect1->nData;
 if (lag>0) imax=imax-lag;

 for (i=imin; i<imax; i++)
 {
   /*
   if (corr->normalize==0)
  corr->present->dataD[maxlag+lag]+=
     v1[maxlag+i]*v2[maxlag+i+lag]/(nData-abs(lag));
  else
   */
  corr->present->dataD[maxlag+lag]+=
     v1[maxlag+i]*v2[maxlag+i+lag]/nData;
  }

 if (corr->normalize==1)
   {
 if (corr->nCall==0)
  corr->average->dataD[maxlag+lag]=
  corr->present->dataD[maxlag+lag];
else
corr->average->dataD[maxlag+lag]=
(corr->average->dataD[maxlag+lag]*corr->nCall+
corr->present->dataD[maxlag+lag])/(corr->nCall+1);
   }
 else{
   oldw=corr->nCall*nData-abs(lag);
   if(oldw<1) oldw=1;
   neww=(corr->nCall+1)*nData-abs(lag);
   if(neww<1) neww=1;
 if (corr->nCall==0)
  corr->average->dataD[maxlag+lag]=
  corr->present->dataD[maxlag+lag]*nData/neww;
else
corr->average->dataD[maxlag+lag]=
(corr->average->dataD[maxlag+lag]*oldw+
nData*corr->present->dataD[maxlag+lag])/neww;
   }

 }

 for (i=0;i<maxlag;i++)
   {
     corr->past1[i]=v1[lv+i-maxlag];
     corr->past2[i]=v2[lv+i-maxlag];
}

 free(v1);
 free(v2);

 corr->nCall++;

 return(0);

}


/*--------------------------------------------------------- FrvCorrInit --*/
int FrvCorrInit(FrvCorr* corr, FrVect *vect1, FrVect *vect2)
/*---------------------------------------------------------------------------*/
/*   Initilizes the corr object: creates all output structures               */
/*   It is automatically called by FrvCorrProc                               */
/*   Returns: 0 in case of success, >0 otherwise                             */
/*---------------------------------------------------------------------------*/
{
   char unitY[256]="";

  if((vect1 == NULL)||(vect2==NULL))
    {sprintf(corr->error,"no input vector");
     return(1);}

 if((vect1->type == FR_VECT_C8)  ||
    (vect1->type == FR_VECT_C16) ||
    (vect1->type == FR_VECT_STRING)||
    (vect2->type == FR_VECT_C8)  ||
    (vect2->type == FR_VECT_C16) ||
    (vect2->type == FR_VECT_STRING))
    {sprintf(corr->error,"Input type not supported: VECT1 %d VECT2 %d",
    vect1->type,vect2->type);
     return(2);}

 if (corr == NULL) return(3);

 if ((vect1->dx[0] != vect2->dx[0])||(vect1->nData != vect2->nData))
             {sprintf(corr->error,"Input vectors mismatch");
     return(2);}


          /*---- init for output space ----*/

  strcat(unitY,vect1->unitY);
  strcat(unitY,"*");
  strcat(unitY,vect2->unitY);
 corr->average  = FrVectNew1D(vect1->name, FR_VECT_8R, 2*corr->maxlag+1,
                               vect1->dx[0], "time [s]",unitY);
 if(corr->average  == NULL)
    {sprintf(corr->error,"malloc in/out failed:ndata=%ld",vect1->nData);
     return(4);}

 corr->present  = FrVectNew1D(vect1->name, FR_VECT_8R, 2*corr->maxlag+1,
                               vect1->dx[0], "time [s]",unitY);
 if(corr->present  == NULL)
    {sprintf(corr->error,"malloc in/out failed:ndata=%ld",vect1->nData);
     return(5);}
 corr->present0=(corr->present->dataD+corr->maxlag);
 corr->average0=(corr->average->dataD+corr->maxlag);
 return(0);

}

/*--------------------------------------------------------- FrvCorrSetPast --*/
void FrvCorrSetPast(FrvCorr* corr, double * past1, double * past2, int flag)
/*-----------------------------------------------------------------------------*/
/* Set corr past times conditions v1_[-n] = past1[n-1]                         */
/* if flag ==0 past times are set to zero.                                     */
/*-----------------------------------------------------------------------------*/
{
int i;
 if (flag==0) 
 {for (i=0;i<corr->maxlag;i++) corr->past1[i]=0;
  for (i=0;i<corr->maxlag;i++) corr->past2[i]=0;}
 else
 {for (i=0;i<corr->maxlag;i++) corr->past1[i]=past1[i];
  for (i=0;i<corr->maxlag;i++) corr->past2[i]=past2[i];}
return;
}


/*---------------------------------------------------------- FrvCorrNew --*/
FrvCorr* FrvCorrNew( int maxlag , int normalize)
/*---------------------------------------------------------------------------*/
/*Creates a new FrvCorr structure                                            */
/*If normalize ==0 the correlation is unbiased.                              */
/*Past values are set to zero.                                               */
/*---------------------------------------------------------------------------*/
{
 FrvCorr *corr;
 int i;

 if (((normalize !=0 )&&(normalize !=1))||(maxlag<=0)) return(NULL);

 /*-------- create the corr structure ----*/


 corr = (FrvCorr *) (calloc(1,sizeof(FrvCorr)));
 if(corr == NULL) return(NULL);

 corr->maxlag=maxlag;
 corr->normalize=normalize;
 corr->past1=(double*) malloc(maxlag*sizeof(double));
 corr->past2=(double*) malloc(maxlag*sizeof(double));



 for (i=0;i<maxlag;i++)
   {corr->past1[i]=0.;
   corr->past2[i]=0.;}




 corr->nCall = 0;

 return(corr);
}







