/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#define _POSIX_SOURCE 
/*---------------------------------------------------------------------------*/
/*  FrvCopy.c by B.Mours LAPP (Annecy)         Jan 12, 2004                  */
/*---------------------------------------------------------------------------*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h> 
#include <stdarg.h>
#include <ctype.h>
#include "FrvCopy.h"

/*---------------------------------define to use FFTW malloc/free functions--*/
#ifdef FFTW_MALLOC
void *fftw_free  (void *p);
void *fftw_malloc(size_t n);
void *FrvCalloc(size_t nobj, size_t size)
{void *mem;
 mem = fftw_malloc(nobj*size);
 if(mem == NULL) return(NULL);
 memset(mem,0,nobj*size);
 return(mem);};
#define malloc fftw_malloc
#define calloc FrvCalloc
#define free   fftw_free
#endif
/*---------------------------------------------------------------------------*/

/*-----------------------------------------------------------------FrvClone--*/
FrVect *FrvClone(FrVect *vectin,
                 char *newName)
/*---------------------------------------------------------------------------*/
/* This function create a new vector (FrVect structure and data area);       */
/* It copy the header information but not the data.                          */
/* If newName = NULL, the original vector name is used.                      */
/* This function returns null in case of problems like malloc failed.        */
/*---------------------------------------------------------------------------*/
{FrVect *vect;
 long i;

  if(vectin == NULL) return(NULL);

   vect = (FrVect *) calloc(1,sizeof(FrVect));
   if(vect == NULL)  
        {return(NULL);}
   else {vect->classe = FrVectDef();}

   if(newName != NULL)
        {if(FrStrCpy(&vect->name,      newName) == NULL) return(NULL);}
   else {if(FrStrCpy(&vect->name, vectin->name) == NULL) return(NULL);}

   vect->compress = 0;
   vect->type     = vectin->type;
   vect->nData    = vectin->nData;
   vect->nBytes   = vectin->nBytes;
   vect->nDim     = vectin->nDim;
   vect->nx     = malloc(vectin->nDim*sizeof(long));
   vect->unitX  = malloc(vectin->nDim*sizeof(char *));
   vect->startX = malloc(vectin->nDim*sizeof(double));
   vect->dx     = malloc(vectin->nDim*sizeof(double));
   if(vect->nx     == NULL || 
      vect->unitX  == NULL || 
      vect->startX == NULL || 
      vect->dx     == NULL) return(NULL);

   for(i=0; i<vectin->nDim; i++)
     {vect->nx[i]     = vectin->nx[i];
      vect->startX[i] = vectin->startX[i];
      vect->dx[i]     = vectin->dx[i];
      FrStrCpy(&vect->unitX[i], vectin->unitX[i]);
      if(vect->unitX[i] == NULL && vectin->unitX[i] != NULL) return(NULL);}


   FrStrCpy(&vect->unitY, vectin->unitY);

   vect->wSize = vectin->wSize;
   vect->space = vectin->space;
   vect->data = malloc(vectin->space*vectin->wSize);
   if(vect->data == NULL) 
      {FrError(3,"FrVectCopy","malloc failed");
       return(NULL);}

   FrVectMap(vect);

   vect->GTime = vectin->GTime;
   vect->next = NULL;

   return(vect);
}

/*-------------------------------------------------------------FrvDecimateD--*/
FrVect *FrvDecimateD(FrVect *vIn,
                     int nGroup,
                     char *newName)
/*---------------------------------------------------------------------------*/
/* This function decimates the data from the vector vIn by averaging nGroup  */
/* values together. The result is put in a new vector named 'newName' of type*/
/* double. The size of the output vector is nGroup time smaller than the size*/
/* of the input vector vect. If newName = NULL, the name of the output       */
/* vector is the same as the input one.                                      */
/*---------------------------------------------------------------------------*/
{FrVect *vect;

 if(vIn == NULL) return(NULL);

 if(newName == NULL) newName = vIn->name;

 vect = FrVectNew1D(newName, FR_VECT_8R, vIn->nData/nGroup, vIn->dx[0]*nGroup, 
                    vIn->unitX[0], vIn->unitY);
 vect = FrVectDecimate(vIn, nGroup, vect);
 vect->GTime = vIn->GTime;

 return(vect);
}
/*-------------------------------------------------------------FrvDecimateF--*/
FrVect *FrvDecimateF(FrVect *vIn,
                     int nGroup,
                     char *newName)
/*---------------------------------------------------------------------------*/
/* This function decimates the data from the vector vIn by averaging nGroup  */
/* values together. The result is put in a new vector named 'newName' of type*/
/* float. The size of the output vector is nGroup time smaller than the size */
/* of the input vector vect. If newName = NULL, the name of the output       */
/* vector is the same as the input one.                                      */
/*---------------------------------------------------------------------------*/
{FrVect *vect;

 if(vIn == NULL) return(NULL);

 if(newName == NULL) newName = vIn->name;

 vect = FrVectNew1D(newName, FR_VECT_4R, vIn->nData/nGroup, vIn->dx[0]*nGroup, 
                    vIn->unitX[0], vIn->unitY);
 vect = FrVectDecimate(vIn, nGroup, vect);
 vect->GTime = vIn->GTime;

 return(vect);
}
/*-------------------------------------------------------------FrvDecimateI--*/
FrVect *FrvDecimateI(FrVect *vIn,
                     int nGroup,
                     char *newName)
/*---------------------------------------------------------------------------*/
/* This function decimates the data from the vector vIn by averaging nGroup  */
/* values together. The result is put in a new vector named 'newName' of type*/
/* int. The size of the output vector is nGroup time smaller than the size   */
/* of the input vector vect. If newName = NULL, the name of the output       */
/* vector is the same as the input one.                                      */
/*---------------------------------------------------------------------------*/
{FrVect *vect;

 if(vIn == NULL) return(NULL);

 if(newName == NULL) newName = vIn->name;

 vect = FrVectNew1D(newName, FR_VECT_4S, vIn->nData/nGroup, vIn->dx[0]*nGroup, 
                    vIn->unitX[0], vIn->unitY);
 vect = FrVectDecimate(vIn, nGroup, vect);
 vect->GTime = vIn->GTime;

 return(vect);
}
/*-------------------------------------------------------------FrvDecimateS--*/
FrVect *FrvDecimateS(FrVect *vIn,
                     int nGroup,
                     char *newName)
/*---------------------------------------------------------------------------*/
/* This function decimates the data from the vector vIn by averaging nGroup  */
/* values together. The result is put in a new vector named 'newName' of type*/
/* short. The size of the output vector is nGroup time smaller than the size */
/* of the input vector vect. If newName = NULL, the name of the output       */
/* vector is the same as the input one.                                      */
/*---------------------------------------------------------------------------*/
{FrVect *vect;

 if(vIn == NULL) return(NULL);

 if(newName == NULL) newName = vIn->name;

 vect = FrVectNew1D(newName, FR_VECT_2S, vIn->nData/nGroup, vIn->dx[0]*nGroup, 
                    vIn->unitX[0], vIn->unitY);
 vect = FrVectDecimate(vIn, nGroup, vect);
 vect->GTime = vIn->GTime;

 return(vect);
}

