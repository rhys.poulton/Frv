/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*------- FrvLinFilt.c   Created May 2001 Author Isidoro Ferrante -----------*/
#include "FrvLinFilt.h"
#include <string.h>
#include <math.h>

int FrvLinFiltProc1(FrvLinFilt *filter, FrVect *vect);
int FrvLinFiltProc2(FrvLinFilt *filter, FrVect *vect);
int FrvLinFiltProc3(FrvLinFilt *filter, FrVect *vect);
int FrvLinFiltProc4(FrvLinFilt *filter, FrVect *vect);
int FrvLinFiltProc5(FrvLinFilt *filter, FrVect *vect);
int FrvLinFiltProc6(FrvLinFilt *filter, FrVect *vect);

/*--------------------------------------------------------- FrvLinFiltFree --*/
void  FrvLinFiltFree(FrvLinFilt* filter)
/*---------------------------------------------------------------------------*/
/*Frees the space allocated to filter                                        */
/*---------------------------------------------------------------------------*/
{
  if (filter->output != NULL && 
      filter->outputIsInput != FR_YES) FrVectFree(filter->output);
  free(filter->a);
  free(filter->b);
  free(filter->xm);
  free(filter->ym);
  free(filter);

  return;
}

/*---------------------------------------------------------- FrvLinFiltProc--*/
int FrvLinFiltProc(FrvLinFilt *filter, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Applies the filter "filter" to the vector "vect"                        */
/*   the output is stored in FrVect* filter->output                          */
/*   it is calculated according to:                                          */
/*   a_0 * y_n = -Sum_(k=1...na) a_k *y_(n-k)+ Sum_(k=0...mb) b_k *x_(n-k)   */
/*   For the first call, initial condition are set to zero: they are recorded*/
/*   for subsequent calls and can be changed with FrvLinFiltSetIni           */
/*---------------------------------------------------------------------------*/
{double input;
 int i;
 int k;

 if(vect    == NULL) return(1);
 if(filter  == NULL) return(2);

 if(filter->outputIsInput == FR_YES)  filter->output = vect;

 if(filter->output == NULL)
   {if(FrvLinFiltInit(filter, vect) == 1) return(3);}

          /*------------------ check that we work on the same buffer length--*/

 if(filter->output->nData != vect->nData)
   {sprintf(filter->error," nData missmatch:%ld %ld\n",
                   filter->output->nData, vect->nData);
    return(4);}

         /*-------------------- set start time for all the series defined ---*/

 filter->output->GTime = vect->GTime;
 filter->nCall++;
 
         /*-----------------------------could we use the optimized code? ---*/

 if(filter->na == filter->mb && 
    filter->na < 8           && 
    filter->outputIsInput == FR_YES) 
   {if     (filter->na == 7) FrvLinFiltProc6(filter, vect);
    else if(filter->na == 6) FrvLinFiltProc5(filter, vect);
    else if(filter->na == 5) FrvLinFiltProc4(filter, vect);
    else if(filter->na == 4) FrvLinFiltProc3(filter, vect);
    else if(filter->na == 3) FrvLinFiltProc2(filter, vect);
    else if(filter->na == 2) FrvLinFiltProc1(filter, vect);
    return(0);}

        /*------------------------------------------------ general code ---*/

 input = 0.;
 for(i=0; i<vect->nData; i++)
   {
   filter->output->dataD[i]=0;
   if     (vect->type == FR_VECT_4R) input = vect->dataF[i];
    else if(vect->type == FR_VECT_8R) input = vect->dataD[i];
    else if(vect->type == FR_VECT_2S) input = vect->dataS[i];
    else if(vect->type == FR_VECT_4S) input = vect->dataI[i];
    else if(vect->type == FR_VECT_8S) input = vect->dataL[i];
    else if(vect->type == FR_VECT_C)  input = vect->data[i];
    else if(vect->type == FR_VECT_2U) input = vect->dataUS[i];
    else if(vect->type == FR_VECT_4U) input = vect->dataUI[i];
    else if(vect->type == FR_VECT_8U) input = vect->dataUL[i];
    else if(vect->type == FR_VECT_1U) input = vect->dataU[i];

    for (k=1;k<filter->na; k++) filter->output->dataD[i]-=filter->a[k]*filter->ym[k-1];

    filter->output->dataD[i]+=filter->b[0]*input;

    for (k=1;k<filter->mb;k++) filter->output->dataD[i]+=filter->b[k]*filter->xm[k-1];
    filter->output->dataD[i]=filter->output->dataD[i] / filter->a[0];
    for (k=filter->na-2;k>0; k--)  filter->ym[k] = filter->ym[k-1];
    for (k=filter->mb-2;k>0; k--)  filter->xm[k] = filter->xm[k-1];
    filter->ym[0]=  filter->output->dataD[i];
    filter->xm[0]=input;
      }

 return(0);
}
/*----------------------------------------------------------FrvLinFiltProc1--*/
int FrvLinFiltProc1(FrvLinFilt *filter, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Applies the filter "filter" to the vector "vect"                        */
/*   the output is stored in input vector. Works only for filter of order 1  */
/*---------------------------------------------------------------------------*/
{double input, a1, b0, b1, out, xm0, ym0;
 int i;
 float *inputF;
 short *inputS;

         /*----------------------------------------- Set local variables ---*/

 b0 = filter->b[0] / filter->a[0];
 b1 = filter->b[1] / filter->a[0];
 a1 =-filter->a[1] / filter->a[0];
 xm0 = filter->xm[0];
 ym0 = filter->ym[0];

 /*---- Compute data. The code is more optimized for short and float --*/ 

 if(vect->type == FR_VECT_4R)
   {inputF  = vect->dataF;
    for(i=0; i<vect->nData; i++)
       {input = inputF[i];
        out = b0*input + b1*xm0 +
                         a1*ym0;
        ym0 = out;
        xm0 = input;
        inputF[i] = out;}}
 else if(vect->type == FR_VECT_2S)
   {inputS  = vect->dataS;
    for(i=0; i<vect->nData; i++)
       {input = inputS[i];
        out = b0*input + b1*xm0 +
                         a1*ym0;    
        ym0 = out;
        xm0 = input;
        inputS[i] = out;}}
 else
   {for(i=0; i<vect->nData; i++)
       {if     (vect->type == FR_VECT_8R) input = vect->dataD[i];
        else if(vect->type == FR_VECT_4S) input = vect->dataI[i];
        else if(vect->type == FR_VECT_8S) input = vect->dataL[i];
        else if(vect->type == FR_VECT_C)  input = vect->data[i];
        else if(vect->type == FR_VECT_2U) input = vect->dataUS[i];
        else if(vect->type == FR_VECT_4U) input = vect->dataUI[i];
        else if(vect->type == FR_VECT_8U) input = vect->dataUL[i];
        else                              input = vect->dataU[i]; 
        out = b0*input + b1*xm0 +
                         a1*ym0;    
        ym0 = out;
        xm0 = input;
        if     (vect->type == FR_VECT_8R) vect->dataD[i] = out;
        else if(vect->type == FR_VECT_4S) vect->dataI[i] = out;
        else if(vect->type == FR_VECT_8S) vect->dataL[i] = out;
        else if(vect->type == FR_VECT_C)  vect->data[i]  = out;
        else if(vect->type == FR_VECT_2U) vect->dataUS[i] = out;
        else if(vect->type == FR_VECT_4U) vect->dataUI[i] = out;
        else if(vect->type == FR_VECT_8U) vect->dataUL[i] = out;
        else if(vect->type == FR_VECT_1U) vect->dataU[i]  = out;}}

                 /*------------------------- stored internal registers ---*/

 filter->xm[0] = xm0;
 filter->ym[0] = ym0;

 return(0);}

/*----------------------------------------------------------FrvLinFiltProc2--*/
int FrvLinFiltProc2(FrvLinFilt *filter, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Applies the filter "filter" to the vector "vect"                        */
/*   the output is stored in input vector. Works only for filter of order 2  */
/*---------------------------------------------------------------------------*/
{double input, a1, a2, b0, b1, b2, out, xm0, xm1, ym0, ym1;
 int i;
 float *inputF;
 short *inputS;

         /*----------------------------------------- Set local variables ---*/

 b0 = filter->b[0] / filter->a[0];
 b1 = filter->b[1] / filter->a[0];
 b2 = filter->b[2] / filter->a[0];

 a1 =-filter->a[1] / filter->a[0];
 a2 =-filter->a[2] / filter->a[0];

 xm0 = filter->xm[0];
 xm1 = filter->xm[1];

 ym0 = filter->ym[0];
 ym1 = filter->ym[1];

 /*---- Compute data. The code is more optimized for short and float --*/ 

 if(vect->type == FR_VECT_4R)
   {inputF  = vect->dataF;
    for(i=0; i<vect->nData; i++)
       {input = inputF[i];
        out = b0*input + b1*xm0 + b2*xm1 +
                         a1*ym0 + a2*ym1;
        ym1 = ym0;    
        ym0 = out;
        xm1 = xm0;
        xm0 = input;
        inputF[i] = out;}}
 else if(vect->type == FR_VECT_2S)
   {inputS  = vect->dataS;
    for(i=0; i<vect->nData; i++)
       {input = inputS[i];
        out = b0*input + b1*xm0 + b2*xm1 +
                         a1*ym0 + a2*ym1;
        ym1 = ym0;    
        ym0 = out;
        xm1 = xm0;
        xm0 = input;
        inputS[i] = out;}}
 else
   {for(i=0; i<vect->nData; i++)
       {if     (vect->type == FR_VECT_8R) input = vect->dataD[i];
        else if(vect->type == FR_VECT_4S) input = vect->dataI[i];
        else if(vect->type == FR_VECT_8S) input = vect->dataL[i];
        else if(vect->type == FR_VECT_C)  input = vect->data[i];
        else if(vect->type == FR_VECT_2U) input = vect->dataUS[i];
        else if(vect->type == FR_VECT_4U) input = vect->dataUI[i];
        else if(vect->type == FR_VECT_8U) input = vect->dataUL[i];
        else                              input = vect->dataU[i]; 
        out = b0*input + b1*xm0 + b2*xm1 +
                         a1*ym0 + a2*ym1;
        ym1 = ym0;    
        ym0 = out;
        xm1 = xm0;
        xm0 = input;
        if     (vect->type == FR_VECT_8R) vect->dataD[i] = out;
        else if(vect->type == FR_VECT_4S) vect->dataI[i] = out;
        else if(vect->type == FR_VECT_8S) vect->dataL[i] = out;
        else if(vect->type == FR_VECT_C)  vect->data[i]  = out;
        else if(vect->type == FR_VECT_2U) vect->dataUS[i] = out;
        else if(vect->type == FR_VECT_4U) vect->dataUI[i] = out;
        else if(vect->type == FR_VECT_8U) vect->dataUL[i] = out;
        else if(vect->type == FR_VECT_1U) vect->dataU[i]  = out;}}

                 /*------------------------- stored internal registers ---*/

 filter->xm[0] = xm0;
 filter->xm[1] = xm1;

 filter->ym[0] = ym0;
 filter->ym[1] = ym1;

 return(0);}

/*----------------------------------------------------------FrvLinFiltProc3--*/
int FrvLinFiltProc3(FrvLinFilt *filter, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Applies the filter "filter" to the vector "vect"                        */
/*   the output is stored in input vector. Works only for filter of order 3  */
/*---------------------------------------------------------------------------*/
{double input, a1, a2, a3, b0, b1, b2, b3, out;
 double xm0, xm1, xm2, ym0, ym1, ym2;
 int i;
 float *inputF;
 short *inputS;

         /*----------------------------------------- Set local variables ---*/

 b0 = filter->b[0] / filter->a[0];
 b1 = filter->b[1] / filter->a[0];
 b2 = filter->b[2] / filter->a[0];
 b3 = filter->b[3] / filter->a[0];

 a1 =-filter->a[1] / filter->a[0];
 a2 =-filter->a[2] / filter->a[0];
 a3 =-filter->a[3] / filter->a[0];

 xm0 = filter->xm[0];
 xm1 = filter->xm[1];
 xm2 = filter->xm[2];

 ym0 = filter->ym[0];
 ym1 = filter->ym[1];
 ym2 = filter->ym[2];

 /*---- Compute data. The code is more optimized for short and float --*/ 

 if(vect->type == FR_VECT_4R)
   {inputF  = vect->dataF;
    for(i=0; i<vect->nData; i++)
       {input = inputF[i];
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2 +
                         a1*ym0 + a2*ym1 + a3*ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        inputF[i] = out;}}
 else if(vect->type == FR_VECT_2S)
   {inputS  = vect->dataS;
    for(i=0; i<vect->nData; i++)
       {input = inputS[i];
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2 +
                         a1*ym0 + a2*ym1 + a3*ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        inputS[i] = out;}}
 else
   {for(i=0; i<vect->nData; i++)
       {if     (vect->type == FR_VECT_8R) input = vect->dataD[i];
        else if(vect->type == FR_VECT_4S) input = vect->dataI[i];
        else if(vect->type == FR_VECT_8S) input = vect->dataL[i];
        else if(vect->type == FR_VECT_C)  input = vect->data[i];
        else if(vect->type == FR_VECT_2U) input = vect->dataUS[i];
        else if(vect->type == FR_VECT_4U) input = vect->dataUI[i];
        else if(vect->type == FR_VECT_8U) input = vect->dataUL[i];
        else                              input = vect->dataU[i]; 
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2+
                         a1*ym0 + a2*ym1 + a3*ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        if     (vect->type == FR_VECT_8R) vect->dataD[i] = out;
        else if(vect->type == FR_VECT_4S) vect->dataI[i] = out;
        else if(vect->type == FR_VECT_8S) vect->dataL[i] = out;
        else if(vect->type == FR_VECT_C)  vect->data[i]  = out;
        else if(vect->type == FR_VECT_2U) vect->dataUS[i] = out;
        else if(vect->type == FR_VECT_4U) vect->dataUI[i] = out;
        else if(vect->type == FR_VECT_8U) vect->dataUL[i] = out;
        else if(vect->type == FR_VECT_1U) vect->dataU[i]  = out;}}

                 /*------------------------- stored internal registers ---*/

 filter->xm[0] = xm0;
 filter->xm[1] = xm1;
 filter->xm[2] = xm2;

 filter->ym[0] = ym0;
 filter->ym[1] = ym1;
 filter->ym[2] = ym2;

 return(0);}

/*----------------------------------------------------------FrvLinFiltProc4--*/
int FrvLinFiltProc4(FrvLinFilt *filter, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Applies the filter "filter" to the vector "vect"                        */
/*   the output is stored in input vector. Works only for filter of order 4  */
/*---------------------------------------------------------------------------*/
{double input, a1, a2, a3, a4, b0, b1, b2, b3, b4, out;
 double xm0, xm1, xm2, xm3, ym0, ym1, ym2, ym3;
 int i;
 float *inputF;
 short *inputS;

         /*----------------------------------------- Set local variables ---*/

 b0 = filter->b[0] / filter->a[0];
 b1 = filter->b[1] / filter->a[0];
 b2 = filter->b[2] / filter->a[0];
 b3 = filter->b[3] / filter->a[0];
 b4 = filter->b[4] / filter->a[0];

 a1 =-filter->a[1] / filter->a[0];
 a2 =-filter->a[2] / filter->a[0];
 a3 =-filter->a[3] / filter->a[0];
 a4 =-filter->a[4] / filter->a[0];

 xm0 = filter->xm[0];
 xm1 = filter->xm[1];
 xm2 = filter->xm[2];
 xm3 = filter->xm[3];

 ym0 = filter->ym[0];
 ym1 = filter->ym[1];
 ym2 = filter->ym[2];
 ym3 = filter->ym[3];

 /*---- Compute data. The code is more optimized for short and float --*/ 

 if(vect->type == FR_VECT_4R)
   {inputF  = vect->dataF;
    for(i=0; i<vect->nData; i++)
     {input = inputF[i];
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2+ b4*xm3 +
                         a1*ym0 + a2*ym1 + a3*ym2+ a4*ym3;
        ym3 = ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm3 = xm2;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        inputF[i] = out;}}
 else if(vect->type == FR_VECT_2S)
   {inputS  = vect->dataS;
    for(i=0; i<vect->nData; i++)
       {input = inputS[i];
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2+ b4*xm3 +
                         a1*ym0 + a2*ym1 + a3*ym2+ a4*ym3;
        ym3 = ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm3 = xm2;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        inputS[i] = out;}}
 else
   {for(i=0; i<vect->nData; i++)
       {if     (vect->type == FR_VECT_8R) input = vect->dataD[i];
        else if(vect->type == FR_VECT_4S) input = vect->dataI[i];
        else if(vect->type == FR_VECT_8S) input = vect->dataL[i];
        else if(vect->type == FR_VECT_C)  input = vect->data[i];
        else if(vect->type == FR_VECT_2U) input = vect->dataUS[i];
        else if(vect->type == FR_VECT_4U) input = vect->dataUI[i];
        else if(vect->type == FR_VECT_8U) input = vect->dataUL[i];
        else                              input = vect->dataU[i]; 
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2+ b4*xm3 +
                         a1*ym0 + a2*ym1 + a3*ym2+ a4*ym3;
        ym3 = ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm3 = xm2;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        if     (vect->type == FR_VECT_8R) vect->dataD[i] = out;
        else if(vect->type == FR_VECT_4S) vect->dataI[i] = out;
        else if(vect->type == FR_VECT_8S) vect->dataL[i] = out;
        else if(vect->type == FR_VECT_C)  vect->data[i]  = out;
        else if(vect->type == FR_VECT_2U) vect->dataUS[i] = out;
        else if(vect->type == FR_VECT_4U) vect->dataUI[i] = out;
        else if(vect->type == FR_VECT_8U) vect->dataUL[i] = out;
        else if(vect->type == FR_VECT_1U) vect->dataU[i]  = out;}}

                 /*------------------------- stored internal registers ---*/

 filter->xm[0] = xm0;
 filter->xm[1] = xm1;
 filter->xm[2] = xm2;
 filter->xm[3] = xm3;

 filter->ym[0] = ym0;
 filter->ym[1] = ym1;
 filter->ym[2] = ym2;
 filter->ym[3] = ym3;

 return(0);}

/*----------------------------------------------------------FrvLinFiltProc5--*/
int FrvLinFiltProc5(FrvLinFilt *filter, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Applies the filter "filter" to the vector "vect"                        */
/*   the output is stored in input vector. Works only for filter of order 5  */
/*---------------------------------------------------------------------------*/
{double input, a1, a2, a3, a4, a5, b0, b1, b2, b3, b4, b5, out;
 double xm0, xm1, xm2, xm3, xm4, ym0, ym1, ym2, ym3, ym4;
 int i;
 float *inputF;
 short *inputS;

         /*----------------------------------------- Set local variables ---*/

 b0 = filter->b[0] / filter->a[0];
 b1 = filter->b[1] / filter->a[0];
 b2 = filter->b[2] / filter->a[0];
 b3 = filter->b[3] / filter->a[0];
 b4 = filter->b[4] / filter->a[0];
 b5 = filter->b[5] / filter->a[0];

 a1 =-filter->a[1] / filter->a[0];
 a2 =-filter->a[2] / filter->a[0];
 a3 =-filter->a[3] / filter->a[0];
 a4 =-filter->a[4] / filter->a[0];
 a5 =-filter->a[5] / filter->a[0];

 xm0 = filter->xm[0];
 xm1 = filter->xm[1];
 xm2 = filter->xm[2];
 xm3 = filter->xm[3];
 xm4 = filter->xm[4];

 ym0 = filter->ym[0];
 ym1 = filter->ym[1];
 ym2 = filter->ym[2];
 ym3 = filter->ym[3];
 ym4 = filter->ym[4];

 /*---- Compute data. The code is more optimized for short and float --*/ 

 if(vect->type == FR_VECT_4R)
   {inputF  = vect->dataF;
    for(i=0; i<vect->nData; i++)
       {input = inputF[i];
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2+ b4*xm3 + b5*xm4 +
                         a1*ym0 + a2*ym1 + a3*ym2+ a4*ym3 + a5*ym4;
        ym4 = ym3;
        ym3 = ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm4 = xm3;
        xm3 = xm2;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        inputF[i] = out;}}
 else if(vect->type == FR_VECT_2S)
   {inputS  = vect->dataS;
    for(i=0; i<vect->nData; i++)
       {input = inputS[i];
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2+ b4*xm3 + b5*xm4 +
                         a1*ym0 + a2*ym1 + a3*ym2+ a4*ym3 + a5*ym4;
        ym4 = ym3;
        ym3 = ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm4 = xm3;
        xm3 = xm2;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        inputS[i] = out;}}
 else
   {for(i=0; i<vect->nData; i++)
       {if     (vect->type == FR_VECT_8R) input = vect->dataD[i];
        else if(vect->type == FR_VECT_4S) input = vect->dataI[i];
        else if(vect->type == FR_VECT_8S) input = vect->dataL[i];
        else if(vect->type == FR_VECT_C)  input = vect->data[i];
        else if(vect->type == FR_VECT_2U) input = vect->dataUS[i];
        else if(vect->type == FR_VECT_4U) input = vect->dataUI[i];
        else if(vect->type == FR_VECT_8U) input = vect->dataUL[i];
        else                              input = vect->dataU[i]; 
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2+ b4*xm3 + b5*xm4 +
                         a1*ym0 + a2*ym1 + a3*ym2+ a4*ym3 + a5*ym4;
        ym4 = ym3;
        ym3 = ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm4 = xm3;
        xm3 = xm2;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        if     (vect->type == FR_VECT_8R) vect->dataD[i] = out;
        else if(vect->type == FR_VECT_4S) vect->dataI[i] = out;
        else if(vect->type == FR_VECT_8S) vect->dataL[i] = out;
        else if(vect->type == FR_VECT_C)  vect->data[i]  = out;
        else if(vect->type == FR_VECT_2U) vect->dataUS[i] = out;
        else if(vect->type == FR_VECT_4U) vect->dataUI[i] = out;
        else if(vect->type == FR_VECT_8U) vect->dataUL[i] = out;
        else if(vect->type == FR_VECT_1U) vect->dataU[i]  = out;}}

                 /*------------------------- stored internal registers ---*/

 filter->xm[0] = xm0;
 filter->xm[1] = xm1;
 filter->xm[2] = xm2;
 filter->xm[3] = xm3;
 filter->xm[4] = xm4;

 filter->ym[0] = ym0;
 filter->ym[1] = ym1;
 filter->ym[2] = ym2;
 filter->ym[3] = ym3;
 filter->ym[4] = ym4;

 return(0);}

/*--------------------------------------------------------- FrvLinFiltProc6--*/
int FrvLinFiltProc6(FrvLinFilt *filter, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Applies the filter "filter" to the vector "vect"                        */
/*   the output is stored in input vector. Works only for filter of order 6  */
/*---------------------------------------------------------------------------*/
{double input, a1, a2, a3, a4, a5, a6, b0, b1, b2, b3, b4, b5, b6, out;
 double xm0, xm1, xm2, xm3, xm4, xm5, ym0, ym1, ym2, ym3, ym4, ym5;
 int i;
 float *inputF;
 short *inputS;

         /*----------------------------------------- Set local variables ---*/

 b0 = filter->b[0] / filter->a[0];
 b1 = filter->b[1] / filter->a[0];
 b2 = filter->b[2] / filter->a[0];
 b3 = filter->b[3] / filter->a[0];
 b4 = filter->b[4] / filter->a[0];
 b5 = filter->b[5] / filter->a[0];
 b6 = filter->b[6] / filter->a[0];

 a1 =-filter->a[1] / filter->a[0];
 a2 =-filter->a[2] / filter->a[0];
 a3 =-filter->a[3] / filter->a[0];
 a4 =-filter->a[4] / filter->a[0];
 a5 =-filter->a[5] / filter->a[0];
 a6 =-filter->a[6] / filter->a[0];

 xm0 = filter->xm[0];
 xm1 = filter->xm[1];
 xm2 = filter->xm[2];
 xm3 = filter->xm[3];
 xm4 = filter->xm[4];
 xm5 = filter->xm[5];

 ym0 = filter->ym[0];
 ym1 = filter->ym[1];
 ym2 = filter->ym[2];
 ym3 = filter->ym[3];
 ym4 = filter->ym[4];
 ym5 = filter->ym[5];

 /*---- Compute data. The code is more optimized for short and float --*/ 

 if(vect->type == FR_VECT_4R)
   {inputF  = vect->dataF;
    for(i=0; i<vect->nData; i++)
       {input = inputF[i];
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2+ b4*xm3 + b5*xm4 + b6*xm5 +
                         a1*ym0 + a2*ym1 + a3*ym2+ a4*ym3 + a5*ym4 + a6*ym5;
        ym5 = ym4;
        ym4 = ym3;
        ym3 = ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm5 = xm4;
        xm4 = xm3;
        xm3 = xm2;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        inputF[i] = out;}}
 else if(vect->type == FR_VECT_2S)
   {inputS  = vect->dataS;
    for(i=0; i<vect->nData; i++)
       {input = inputS[i];
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2+ b4*xm3 + b5*xm4 + b6*xm5 +
                         a1*ym0 + a2*ym1 + a3*ym2+ a4*ym3 + a5*ym4 + a6*ym5;
        ym5 = ym4;
        ym4 = ym3;
        ym3 = ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm5 = xm4;
        xm4 = xm3;
        xm3 = xm2;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        inputS[i] = out;}}
 else
   {for(i=0; i<vect->nData; i++)
       {if     (vect->type == FR_VECT_8R) input = vect->dataD[i];
        else if(vect->type == FR_VECT_4S) input = vect->dataI[i];
        else if(vect->type == FR_VECT_8S) input = vect->dataL[i];
        else if(vect->type == FR_VECT_C)  input = vect->data[i];
        else if(vect->type == FR_VECT_2U) input = vect->dataUS[i];
        else if(vect->type == FR_VECT_4U) input = vect->dataUI[i];
        else if(vect->type == FR_VECT_8U) input = vect->dataUL[i];
        else                              input = vect->dataU[i]; 
        out = b0*input + b1*xm0 + b2*xm1 + b3*xm2+ b4*xm3 + b5*xm4 + b6*xm5 +
                         a1*ym0 + a2*ym1 + a3*ym2+ a4*ym3 + a5*ym4 + a6*ym5;
        ym5 = ym4;
        ym4 = ym3;
        ym3 = ym2;
        ym2 = ym1;
        ym1 = ym0;    
        ym0 = out;
        xm5 = xm4;
        xm4 = xm3;
        xm3 = xm2;
        xm2 = xm1;
        xm1 = xm0;
        xm0 = input;
        if     (vect->type == FR_VECT_8R) vect->dataD[i] = out;
        else if(vect->type == FR_VECT_4S) vect->dataI[i] = out;
        else if(vect->type == FR_VECT_8S) vect->dataL[i] = out;
        else if(vect->type == FR_VECT_C)  vect->data[i]  = out;
        else if(vect->type == FR_VECT_2U) vect->dataUS[i] = out;
        else if(vect->type == FR_VECT_4U) vect->dataUI[i] = out;
        else if(vect->type == FR_VECT_8U) vect->dataUL[i] = out;
        else if(vect->type == FR_VECT_1U) vect->dataU[i]  = out;}}

                 /*------------------------- stored internal registers ---*/

 filter->xm[0] = xm0;
 filter->xm[1] = xm1;
 filter->xm[2] = xm2;
 filter->xm[3] = xm3;
 filter->xm[4] = xm4;
 filter->xm[5] = xm5;

 filter->ym[0] = ym0;
 filter->ym[1] = ym1;
 filter->ym[2] = ym2;
 filter->ym[3] = ym3;
 filter->ym[4] = ym4;
 filter->ym[5] = ym5;

 return(0);}


/*--------------------------------------------------------- FrvLinFiltInit --*/
int FrvLinFiltInit(FrvLinFilt* filter, FrVect *vect)
/*---------------------------------------------------------------------------*/
/*   Initilizes the filter object: creates all output structures             */
/*   Returns: 0 in case of success, 1 otherwise                              */
/*---------------------------------------------------------------------------*/
{
 if (filter == NULL) return(3);

 if(vect == NULL)
    {sprintf(filter->error,"no input vector");
     return(1);}

 if((vect->type == FR_VECT_C8)  ||
    (vect->type == FR_VECT_C16) ||
    (vect->type == FR_VECT_STRING))
    {sprintf(filter->error,"Input type not supported:%d",vect->type);
     return(2);}


          /*---- init for output space ----*/

 filter->output  = FrVectNew1D(vect->name, FR_VECT_8R, vect->nData,
                               vect->dx[0], "time [s]",vect->unitY);
 if(filter->output  == NULL)
    {sprintf(filter->error,"malloc in/out failed:ndata=%ld",vect->nData);
     return(4);}

 return(0);

}

/*--------------------------------------------------------- FrvLinFiltSetIni --*/
void FrvLinFiltSetIni(FrvLinFilt* filter, double * ym, double * xm)
/*-----------------------------------------------------------------------------*/
/* Set filter initial conditions x_n, n= -1.....-(na-1) y_m, m = -1....-(mb-1)*/
/* These are stored in the order ym[0]=y_-1, ym[1]=y_-2....ym[na-2]=y_na-1     */
/*                               xm[0]=x_-1, xm[1]=x_-2....xm[na-2]=x_mb-1     */
/*-----------------------------------------------------------------------------*/
{
int i;
 for (i=0;i<filter->na-1;i++) filter->ym[i]=ym[i];
 for (i=0;i<filter->mb-1;i++) filter->xm[i]=xm[i];
return;
}

/*--------------------------------------------------------- FrvLinFiltSetGain --*/
void FrvLinFiltSetGain(FrvLinFilt* filter, double freq, double gain )
/*-----------------------------------------------------------------------------*/
     /* Sets filter gain at frequency freq  */
     /* freq is in sampling frequency units: */
/*-----------------------------------------------------------------------------*/
{
  double num=0, rnum=0, inum=0;
  double den=0, rden=0, iden=0;
  double fase, pi=acos(-1.);
  int i=0;
  int sign=-1;
  fase=2*pi*freq;
  if (freq==0.)
    {for (i=0;i<filter->mb;i++) num+=filter->b[i];
     for (i=0;i<filter->na;i++) den+=filter->a[i];
  }
  else if (freq==0.5)
    {
     for (i=0;i<filter->mb;i++) num+=pow(sign,i)*filter->b[i];
     for (i=0;i<filter->na;i++) den+=pow(sign,i)*filter->a[i];
    }
  else
 {
     for (i=0;i<filter->mb;i++)
     {rnum+=filter->b[i]*cos(i*fase);
      inum+=filter->b[i]*sin(i*fase);}
     for (i=0;i<filter->na;i++)
     {rden+=filter->a[i]*cos(i*fase);
      iden+=filter->a[i]*sin(i*fase);}
     num=sqrt(pow(rnum,2)+pow(inum,2));
     den=sqrt(pow(rden,2)+pow(iden,2));
}
     if (num==0.)
     {sprintf(filter->error,"Cannot set gain :%f %f ",freq,gain);
      return;
     }

     for (i=0;i<filter->mb;i++) filter->b[i]=(gain*den/num)*filter->b[i];
     return;
}

/*---------------------------------------------------------- FrvLinFiltNew --*/
FrvLinFilt* FrvLinFiltNew(double * a, double * b, int na, int mb)
/*---------------------------------------------------------------------------*/
/*Creates a new filter                                                       */
/*Initial conditions are set to zero.                                        */
/*---------------------------------------------------------------------------*/
{
 FrvLinFilt *filter;

 int i;
 if (a[0]==0) return(NULL);

 /*-------- create the filter structure ----*/


 filter = (FrvLinFilt *) (calloc(1,sizeof(FrvLinFilt)));
 if(filter == NULL) return(NULL);

 filter->b=(double*) malloc(mb*sizeof(double));
 filter->a=(double*) malloc(na*sizeof(double));
 filter->xm=(double*) malloc((mb-1)*sizeof(double));
 filter->ym=(double*) malloc((na-1)*sizeof(double));
 if ((filter->a==NULL)||(filter->b==NULL)||((mb>1)&&(filter->xm==NULL))||((na>1)&&(filter->ym==NULL)))
 return(NULL);

 for (i=0;i<na;i++) filter->a[i]=a[i];
 for (i=0;i<mb;i++) filter->b[i]=b[i];
 for (i=0;i<na-1;i++) filter->ym[i]=0;
 for (i=0;i<mb-1;i++) filter->xm[i]=0;

 filter->mb=mb;
 filter->na=na;

 filter->nCall = 0;
 filter->outputIsInput = FR_NO;

 return(filter);
}

FrvLinFilt* FrvLinFiltMult(FrvLinFilt* filter2, FrvLinFilt* filter1)
/*---------------------------------------------------------------------------*/
/*Multiplies filter1 and filter2, and creates a new filter. If necessary,    */
/*old filters can be freed by the user.                                      */
/*---------------------------------------------------------------------------*/
{
  FrvLinFilt * filter;
  int i,k,kmax,kmin;
  int na,mb;
  double *a;
  double *b;


  if ( filter1 == NULL) return(NULL);
  if ( filter2 == NULL) return(NULL);
  na=filter1->na + filter2->na - 1;
  mb=filter1->mb + filter2->mb - 1;
  a=(double*) malloc(na*sizeof(double));
  b=(double*) malloc(mb*sizeof(double));

  for (i=0; i<na; i++)
    { a[i]=0;
    kmin=0;
    if (i>=filter2->na) kmin=i-filter2->na+1;
    kmax=filter1->na-1;
    if ((i-kmax)<0) kmax=i;

    for (k=kmin; k<=kmax; k++)
      a[i]+=filter2->a[i-k]*filter1->a[k];
    }
  for (i=0; i<mb; i++)
    { b[i]=0;
    kmin=0;
    if (i>=filter2->mb) kmin=i-filter2->mb+1;
    kmax=filter1->mb-1;
    if ((i-kmax)<0) kmax=i;

      for (k=kmin; k<=kmax; k++)
      b[i]+=filter2->b[i-k]*filter1->b[k];
    }

 filter=FrvLinFiltNew(a,b,na,mb);
 free(a);
 free(b);
  return(filter);
}

FrvLinFilt * FrvLinFiltCopy(FrvLinFilt * filter0)
/*---------------------------------------------------------------------------*/
/*Copies a filter to a new one. Filter output is not initialized.            */
/*---------------------------------------------------------------------------*/

{
  FrvLinFilt * filter;
  if (filter0 == NULL) return(NULL);
  filter = FrvLinFiltNew(filter0->a,filter0->b,filter0->na,filter0->mb);
  return(filter);
}
FrvLinFilt * FrvLinFiltAddZP(FrvLinFilt * filter0, double fase, double modulo, int type)
/*---------------------------------------------------------------------------*/
/* Creates a new filter adding a single real zero or pole or a couple of     */
/* complex conjugates ones in the Z plane                                    */
/* fase  : zero-pole phase, in units of 2*pi,                                */
/*          or frequency in units of sampling frequency                      */
/* If it is zero or 0.5, only one is zero or pole is  added.                 */
/* modulo: zero-pole module. If it is a pole, and if modulo>1,               */
/*         the filter is unstable                                            */
/* type: 0 adds a zero                                                       */
/* type: 1 adds a pole only if it is stable                                  */
/* type: 2 adds a pole stable or not.                                        */
/*---------------------------------------------------------------------------*/
  {int na,mb,n2;
   int i,k,kmin,kmax;
   double *a;
   double *b;
  double v2[3];
  double pi=acos(-1.);
  FrvLinFilt * filter;
  /*Check if the input filter exists.*/
  if (filter0 == NULL) return(NULL);
  /*Check is the input type and module are reasonable*/
  if ((type>2)||(type<0)||(modulo<=0)) return(NULL);
  /*Check if the pole is stable*/
  if ((type==1)&&(modulo>1)) return(NULL);
  /* if fase = 0 adds only one zero/pole*/
  if ((fase ==0.)||(fase==0.5))
  {
  n2=2;
  v2[0]=1;
  v2[1]=-modulo;
  }
  else
  {n2=3;
  v2[0]=1;
  v2[1]=-2*modulo*cos(fase*2*pi);
  v2[2]=modulo*modulo;
  }
  if (type ==0)
  {
  mb=(filter0->mb+n2-1);
  na=filter0->na;
  a=(double*) malloc(na*sizeof(double));
  b=(double*) malloc(mb*sizeof(double));
  for (i=0;i<na;i++) a[i]=filter0->a[i];
    for (i=0;i<mb;i++)
    { b[i]=0;
    kmin=0;
    if (i>=n2) kmin=i-n2+1;
    kmax=filter0->mb-1;
    if ((i-kmax)<0) kmax=i;

    for (k=kmin; k<=kmax; k++)
      b[i]+=v2[i-k]*filter0->b[k];
    }
}
  else
  {
  mb=filter0->mb;
  na=(filter0->na+n2-1);
  a=(double*) malloc(na*sizeof(double));
  b=(double*) malloc(mb*sizeof(double));
  for (i=0;i<mb;i++) b[i]=filter0->b[i];
       for (i=0;i<na;i++)
       { a[i]=0;
       kmin=0;
       if (i>=n2) kmin=i-n2+1;
       kmax=filter0->na-1;
       if ((i-kmax)<0) kmax=i;

       for (k=kmin; k<=kmax; k++)
         a[i]+=v2[i-k]*filter0->a[k];
       }
   }

  filter=FrvLinFiltNew(a,b,na,mb);


  free(a);
  free(b);
  return(filter);
   }
  int FrvButtOrder(double fp,double fs,double tp,double ts,double *wc)
/*****************************************************************
Calculates order of a low pass Butterworth filter, plus parameter wc.
Input:
fp upper edge of pass band in units of sampling frequency.
fs lower edge of stop band in units of sampling frequency
tp value of transfer function modulus at fp.
ts value of trensfer function modulus at fs.
Output:
Filter order N
wc parameter for butterworth filter.
Equals to tan(pi*fc) where fc is the frequency, in units of fs, were the 
transfer function modulus equals to sqrt(2).
*****************************************************************/
{
  double pi,epsilon2,A2,n, wp, ws;
  int N;
if ((fp==0)||(fs==0)||(tp<=0)||(tp>=1)||(ts<=0)||(ts>=1)||(fp>fs)||(ts>tp)) 
          return 0; 
  pi=2.*asin(1.);
  wp=tan(pi*fp);
  ws=tan(pi*fs);
  epsilon2=(1-tp*tp)/tp*tp;
  A2=1/(ts*ts);
  n=.5*log(epsilon2/(A2-1))/log(wp/ws);
  N=ceil(n);
  wc[0]=wp/pow(epsilon2,.5/N);
  return N;

}

void FrvButtFilt(int N, double wc, double * a, double * b)
/*****************************************************************************
Given the filter order N and the parameters wc (see function FrvButtOrder) 
for explanation) calculates lowpass filter coefficients a,b.
******************************************************************************/

{ double a0[3],b0[3];
 double * at,* bt;
 double x;
 int Npairs,k,ind,ind1;
 at=(double*)calloc((N+1),sizeof(double));
 bt=(double*)calloc((N+1),sizeof(double));
 a[0]=1;
 b[0]=1;
 for (k=1;k<=N;k++){a[k]=0;b[k]=0;}
 /* number of zero pairs*/
 Npairs = floor(N/2.);
 for (k=0; k<Npairs; k++)
   {FrvButtCell(wc,k,N,a0,b0);
   /*vectors a and a0, b and b0 are convoluted: the result is placed
     in a temporary vector*/
   for (ind=0;ind<=N;ind++)
   { ind1=0;
        while((ind1<=ind)&(ind1<3))
	 {at[ind]+=a[ind-ind1]*a0[ind1];
	 bt[ind]+=b[ind-ind1]*b0[ind1];
         ind1++;}
  }
   /* The temporary vectors at, bt, are copied in a, b and then rest to zero*/
   for (ind=0;ind<=N;ind++)
     { a[ind]=at[ind];
     at[ind]=0;
     b[ind]=bt[ind];
     bt[ind]=0;
     }

   }
 /* if the filter order is odd, a term is addedd*/
x=N/2.-ceil(N/2.);
if (x!=0.)
   {
     a0[0]=wc+1;
     a0[1]=wc-1;
     a0[2]=0;
     b0[0]=wc;
     b0[1]=wc;
     b0[2]=0;
    for (ind=0;ind<=N;ind++)
       {ind1=0;
   while((ind1<=ind)&(ind1<3))
	 {at[ind]+=a[ind-ind1]*a0[ind1];
	 bt[ind]+=b[ind-ind1]*b0[ind1];
         ind1++;}
      }
     /* copio il vettore temporaneo in quello definitivo e
	azzero il vettore temporaneo*/
     for (ind=0;ind<=N;ind++)
       { a[ind]=at[ind];
       at[ind]=0;
       b[ind]=bt[ind];
       bt[ind]=0;
       }
   }
 free(at);
 free(bt);
 return;
}

void  FrvButtCell(double wc, int k, int N, double * a, double * b)
     /*********************************
Calculates the coefficient of an elemntary second order cell of a butterworth filter.
For internal use only.
     **********************************/
{ double pi, sp;
 pi=2.*asin(1.);
 sp=sin(pi*(2*k+1)/(2.*N));
 a[0]=1.+wc*wc+wc*2*sp;
 a[1]=2.*(wc*wc-1);
 a[2]=1+wc*wc-wc*2*sp;
 b[0]=wc*wc;
 b[1]=2.*wc*wc;
 b[2]=wc*wc;
 return;
}

 FrvLinFilt * FrvLinFiltButt( double fp, double fs, double tp, double ts, int order, double fc)
/***************************************************************************
Creates a FrvlinFilt object consinsting of a low pass butterworth filter.
You can specify the filter using the two set of parameters order, fc
or fp,fs,tp,ts.

if order>0, a filter of order "order" with a -3dB frequency of fc is created.
other inputs are ignored.

if order=0  the filter order is calculated according to the parameters 
fp,fs,tp,ts, where:
fp upper edge of pass band in units of sampling frequency.
fs lower edge of stop band in units of sampling frequency
tp value of transfer function modulus at fp.
ts value of trensfer function modulus at fs.

if order<0 and all the other parameters are different from zero, then the
minimum order between the two given possibilities is chosen.
This option could be used to give an upper limit to filter length.

***************************************************************************/

 {double *a, *b;
  double wc,wc_calc;
  int N_calc,N;
  FrvLinFilt* Filter;

  N_calc = -1;
  wc     = 0;
  N      = 0;

  /* if we give the filter order, fc should have a reasonable value */
  if ((order!=0)&&((fc<=0)||(fc>=1))) return NULL;

  if (order>0)
    {  
     N=order;
     wc=tan(2*fc*asin(1.));
    }
   else
     {  
      N_calc=FrvButtOrder(fp,fs,tp,ts,&wc_calc);
     }

if (N_calc==0) return NULL;

   if (order==0)
     {N=N_calc;
     wc=wc_calc;
     }
   else if (order<0)
 {
 if (-1*order<N_calc) 
 {N=-1*order;
  wc=tan(2*fc*asin(1.));
 }
 else 
{N=N_calc;
 wc=wc_calc;
}
 }


   a=(double*) calloc((N+1),sizeof(double));
   b=(double*) calloc((N+1),sizeof(double));
   FrvButtFilt(N,wc,a,b);
   Filter= FrvLinFiltNew(a,b,N+1,N+1);
   free(a);
   free(b);
   return(Filter);

 }

void FrvLinFiltButtLowToHigh(FrvLinFilt * filter)
     /**************************************************************
      Transforms a low pass Butterworth filter into an high pass one
     **************************************************************/
{
  int i;
  int sign=1;
  if (filter==NULL)return ;
  for (i=0;i<filter->mb;i++)
    {filter->b[i]=filter->b[i]*sign;
    sign=-sign;
    }
  FrvLinFiltSetGain(filter,0.5,1.);
  return;
}

