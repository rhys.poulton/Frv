/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#define _POSIX_SOURCE
/*---------------------------------------------------------------------------*/
/*  FrvVersion.c by B.Mours LAPP (Annecy)/Caltech                            */
/*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include "Frv.h"

/*------------------------------------------------------------FrvLibVersion--*/
void FrvLibVersion(FILE *fOut)
/*---------------------------------------------------------------------------*/
{

 if(fOut == NULL) fOut = stdout;

 fprintf(fOut,"  Frv    Version:%s %s(Compiled: %s %s)\n",
                               FRV_VERSION, FRV_PATH, __DATE__, __TIME__);

 return;}
