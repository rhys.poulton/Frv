/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*--------------  FrvBuf.h by B.Mours Jul 07, 2004--------------------------*/

#ifndef FRVCH
#define FRVCH

#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif

#include "Frv.h"

#ifdef __cplusplus
extern "C" {
#endif

struct FrvCh {        /* This is the FrVect Buffering object           */
  char *name;         /* vector or SerData name                        */
  char *smsParam;     /* serData parameter name (if isSerData=YES)     */
  double duration;    /* requested length for the vector output        */
  double stillToFeed; /* length of the missing data segement           */
  double lastGPS;     /* end time of the last input data segement      */
  int    dt;          /* period for finding the data in a frame        */
  FRBOOL init;        /* tell if the channel type has been determined  */
  FRBOOL isSerData;   /* tell if it is a SerData                       */
  FrvBuf *buffer;     /* buffer used for vector if not a SerData       */
  FrVect *output;     /* output vector                                 */
};

typedef struct FrvCh FrvCh;

void    FrvChFree    (FrvCh* frCh);
double  FrvChFeed    (FrvCh *frCh, FrameH *frame);
FrVect* FrvChGetVect (FrvCh* frCh);
FrvCh*  FrvChNew     (char *channelName, double duration);

#ifdef __cplusplus
}
#endif

#endif
