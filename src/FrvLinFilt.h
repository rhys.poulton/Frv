/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef FRVLINFILT
#define FRVLINFILT

#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif
/*---------------------------------------------------------------------------*/
/*  FrvLinFilt.h Created May 2001 Author Isidoro Ferrante                    */
/*---------------------------------------------------------------------------*/

#include "FrameL.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct FrvLinFilt FrvLinFilt;

struct FrvLinFilt {
                              /*--------------------- input data -------------*/
  double * a;                 /* filter coefficients (see definition below)   */
  double * ym;                /* internal coefficient */
  double * b;                 /* filter coefficients (see definition below)   */
  double * xm;                /* internal coefficient */
  int na;
  int mb;
  struct FrVect *output;     /* vector to hold the last data chunk           */
  char error[256];           /* hold error message if any                    */
  int nCall;                 /* number of call for the algorithms            */
  char *name;                 /* name to be use to build linked list         */
  FRBOOL outputIsInput;      /* if FR_YES the output vector is the input one */
  FrvLinFilt *next;          /* to build linked list                         */
};


void        FrvLinFiltFree(FrvLinFilt* filter);
int         FrvLinFiltProc(FrvLinFilt *filter, FrVect *vect);
int         FrvLinFiltInit(FrvLinFilt* filter, FrVect *vect);
FrvLinFilt *FrvLinFiltNew(double * a, double * b, int na, int mb);
void        FrvLinFiltSetIni(FrvLinFilt* filter, double * ym, double * xm);
FrvLinFilt *FrvLinFiltMult(FrvLinFilt* filter2, FrvLinFilt* filter1);
FrvLinFilt *FrvLinFiltCopy(FrvLinFilt * filter0);
FrvLinFilt *FrvLinFiltAddZP(FrvLinFilt * filter0, double fase, double modulo, int type);
void        FrvLinFiltSetGain(FrvLinFilt* filter, double freq, double gain );
int         FrvButtOrder(double fp,double fs,double tp,double ts,double *wc);
void        FrvButtFilt(int N, double wc, double * a, double * b);
void        FrvButtCell(double wc, int k, int N, double * a, double * b);
FrvLinFilt *FrvLinFiltButt( double fp, double fs, double tp, double ts, int order, double fc);
void        FrvLinFiltButtLowToHigh(FrvLinFilt * filter);
#ifdef __cplusplus
}
#endif

#endif
