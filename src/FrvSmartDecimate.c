/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */


#include "FrvSmartDecimate.h"
#include <math.h>
FrvSmartD *FrvSmartDInit(int factor,double f0,double fmax, double tp, double treject)
{
 int steps[100];
  int nsteps=0;
  int div=2;
  int pfact=factor;
  int k;
  double fo,fn,fp,fs;

  FrvSmartD * filterbank=NULL;
  /* a check on the input parameters */
  if ((tp<0)||(tp>1)) return NULL;
  if ((treject<0)||(treject>1)||(treject>tp)) return NULL;
  if ((fmax<=0)||(fmax>=.5*f0/factor)) return NULL;


  filterbank=(FrvSmartD *) calloc(1,sizeof(FrvSmartD));

  filterbank->factor=factor;
   while (pfact!=1)
          {if (fmod(pfact,div) ==0)
{steps[nsteps]=div;
nsteps++;
pfact=pfact/div;}
else div++;
}
filterbank->nsteps=nsteps;
filterbank->steps=(int*)calloc(nsteps,sizeof(int));
fo=f0;
fn=f0;



 filterbank->bank=(FrvLinFilt**) calloc(nsteps,sizeof(FrvLinFilt*));
 if (nsteps>1)  
 filterbank->vectors = (FrVect**) calloc((nsteps-1),sizeof(FrVect*));
 else   filterbank->vectors = NULL;

for (k=0; k<nsteps;k++)
{
fo=fn;
filterbank->steps[k]=steps[nsteps-k-1];

fn=fo/steps[nsteps-k-1];
/*
fp=.8*.5*ff/fo;
fs=1.2*.5*fn/fo;
*/
 fp=fmax/fo;
 fs=(fn-fmax)/fo;
filterbank->bank[k]= FrvLinFiltButt(fp,fs,tp,treject,0,0.);
/*printf("%d  %f %f %f %f \n",filterbank->steps[k],fp,fs,tp,ts);*/
}
/*
printf("Finito!\n");
*/
filterbank->output=NULL;
filterbank->input=NULL;
return(filterbank);

/*
printf("tornato!\n");
*/
}


void FrvSmartDFree(FrvSmartD* filterbank)
{
int k;
if (filterbank==NULL) return;
/*
printf("%i\n",filterbank->steps[0]);
*/
for (k=0; k<filterbank->nsteps;k++)
if (filterbank->bank[k]!=NULL)

  /*
{printf("%i\n",k);
*/

FrvLinFiltFree(filterbank->bank[k]);

/*
}
*/

if (filterbank->steps!=NULL) free(filterbank->steps);
/*
printf("quasi fatto \n");
*/

free(filterbank);

/*
printf("fatto!\n");
*/

return;
}

FrVect * FrvSmartDProc(FrvSmartD * bank, FrVect * Vin, FrVect * Vout)
{
  int k, out_nData, Index,Indexd,step;
  double out_dx;
  /* Dapprima verifico che esistano Vin, Vout.
   */
  if ((Vin==NULL)&&(bank->input==NULL)) return NULL;
  /*funziona solo con vettori monodimensionali*/
  /*  printf("e uno!\n");*/
  if (Vin != NULL) bank->input=Vin;
  if (bank->input->nDim!=1) return NULL;
  /* verifico che la lunghezza del vettore sia divisibile per factor*/
  /*  printf("e due!\n");*/
  if (fmod(bank->input->nData,bank->factor)!=0) return NULL;
  /* printf("e tre!\n");*/



  if (Vout==NULL)
    {
      /*Se il vettore in uscita non esiste, controllo dapprima se esiste o meno il puntatore*/
      if (bank->output==NULL){
        out_nData=bank->input->nData/bank->factor;
        out_dx=bank->input->dx[0]*bank->factor;
	/*        out_nx=bank->input->nx[0]/bank->factor; */
	bank->output = FrVectNew1D(bank->input->name,bank->input->type,out_nData,out_dx,bank->input->unitX[0],bank->input->unitY);
	/*printf("e quattro!\n");*/
      }
    }
  else bank->output=Vout;
  /*
Controllo se esistono i vettori in ingresso-uscita. Se no, li creo
   */

  if (bank->nsteps>1)

  {if (bank->vectors[0]==NULL)
    { 
      out_nData=bank->input->nData;
      out_dx=bank->input->dx[0];
      for (k=0;k<bank->nsteps-1;k++)
	{ step=bank->steps[k];
          out_nData=out_nData/step;
          out_dx=out_dx*step;
	  bank->vectors[k]=FrVectNew1D(bank->input->name,FR_VECT_8R,out_nData,out_dx,bank->input->unitX[0],bank->input->unitY);
	  /*  printf("e cinque! % i \n",k);*/
         }
    }
  }
  for (k=0;k<bank->nsteps;k++)
    {
      step=bank->steps[k];
      
      if ((k==0)&&(bank->nsteps>1)) {
FrvLinFiltProc(bank->bank[k],bank->input);
 for (Index=0;Index<bank->vectors[k]->nData;Index++)
   {Indexd=(Index+1)*step-1;
   bank->vectors[k]->dataD[Index]=bank->bank[k]->output->dataD[Indexd];}   
   }
      else if (k==(bank->nsteps)-1) {

if (bank->nsteps>1) FrvLinFiltProc(bank->bank[k],bank->vectors[k-1]);
  else FrvLinFiltProc(bank->bank[k],bank->input);
 for (Index=0;Index<bank->output->nData;Index++)
   {Indexd=(Index+1)*step-1;
    if     (bank->output->type == FR_VECT_4R)  
   bank->output->dataF[Index]=bank->bank[k]->output->dataD[Indexd];

    else if(bank->output->type == FR_VECT_8R) 
   bank->output->dataD[Index]=bank->bank[k]->output->dataD[Indexd];

    else if(bank->output->type == FR_VECT_2S)  
   bank->output->dataS[Index]=bank->bank[k]->output->dataD[Indexd];

    else if(bank->output->type == FR_VECT_4S)  
   bank->output->dataI[Index]=bank->bank[k]->output->dataD[Indexd];

    else if(bank->output->type == FR_VECT_8S)  
   bank->output->dataL[Index]=bank->bank[k]->output->dataD[Indexd];

    else if(bank->output->type == FR_VECT_C)   
   bank->output->data[Index]=bank->bank[k]->output->dataD[Indexd];

    else if(bank->output->type == FR_VECT_2U)  
   bank->output->dataUS[Index]=bank->bank[k]->output->dataD[Indexd];

    else if(bank->output->type == FR_VECT_4U)  
   bank->output->dataUI[Index]=bank->bank[k]->output->dataD[Indexd];

    else if(bank->output->type == FR_VECT_8U)  
   bank->output->dataUL[Index]=bank->bank[k]->output->dataD[Indexd];

    else if(bank->output->type == FR_VECT_1U)  
    bank->output->dataU[Index]=bank->bank[k]->output->dataD[Indexd];
   }
   }
      else{
FrvLinFiltProc(bank->bank[k],bank->vectors[k-1]);
 for (Index=0;Index<bank->vectors[k]->nData;Index++)
   {Indexd=(Index+1)*step-1;
   bank->vectors[k]->dataD[Index]=bank->bank[k]->output->dataD[Indexd];

  }

 }



    }




  return(bank->output);
}

void FrvSmartDReset( FrvSmartD * filterbank, double value)
{
  int istep, ia,ib;
  for (istep=0;istep<filterbank->nsteps;istep++)
    {for (ia=0; ia<filterbank->bank[istep]->na-1 ;ia++)
      filterbank->bank[istep]->ym[ia]=value;
     for (ib=0; ib<filterbank->bank[istep]->mb-1 ;ib++)
      filterbank->bank[istep]->xm[ib]=value;
     }
}



