/* SPDX-License-Identifier: LGPL-2.1-or-later
 *
 * Frv - Frame vector library
 *
 * Copyright © 2001-2021 Laboratoire d'Annecy de physique des particules - CNRS
 * Copyright © 2019-2021 Institut pluridisciplinaire Hubert CURIEN - CNRS
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/*---------------------------------------------------------------------------*/
/* FrvTFTest.c      Jan 8, 2001                                              */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Frv.h" 

/*---------------------------------------------------------------------Main--*/
int main(int argc, char *argv[])  
/*---------------------------------------------------------------------------*/
{FrvRFFT *fft1, *fft2;
 FrvTF *tf;
 FrvFilter2 *filter;
 FrVect *vect;
 int nData, iVect, i;
 double dt, phase = 0;

 nData = 500;
 dt = 5./nData;
 vect = FrVectNew1D("rand()/RAND_MAX+ sin(phase)",FR_VECT_8R, nData,dt,
                                            "time [s]","Volts");

 fft1 = FrvRFFTNew("HAP", 0, 0);
 fft2 = FrvRFFTNew("HAP", 0, 0);

 filter = FrvFilter2New(0.,0.,1.,1./(6.14*30*6.14*30),1./(6.14*30.),1.);

 tf = FrvTFNew(NULL, 0, 0);

 for(iVect = 0; iVect<100; iVect++) {/*----------- we loop on many vectors--*/
 
   for(i=0; i<nData; i++)  /*---------- update the input vector ----------*/
      {phase+= 6.2831853*7.*dt;
       vect->dataD[i] = ((double) rand())/RAND_MAX+ sin(phase);}

   FrvFilter2Proc(filter, vect);

   FrvTFProc(tf, filter->output, vect);

   FrvRFFTFor(fft1, vect);
   FrvRFFTFor(fft2, filter->output);
   
   FrVectDump(vect,stdout,2);
   FrVectDump(tf->output, stdout, 2);
   FrVectDump(tf->modulus, stdout, 2);}

 return(0);}
